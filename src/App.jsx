import React, { useState, useRef } from 'react';

function Timer() {
  const [time, setTime] = useState(0);
  const [isRunning, setIsRunning] = useState(false);
  const [savedTimes, setSavedTimes] = useState([]);

  const MAX_SAVED_TIMES = 5;
  const intervalRef = useRef(null);

  const formatTime = (time) => {
    const hours = Math.floor(time / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    const seconds = time % 60;

    return (
      String(hours).padStart(2, '0') +
      ':' +
      String(minutes).padStart(2, '0') +
      ':' +
      String(seconds).padStart(2, '0')
    );
  };

  const startTimer = () => {
    setIsRunning(true);
    intervalRef.current = setInterval(() => {
      setTime((prevTime) => prevTime + 1);
    }, 1000);
  };

  const pauseTimer = () => {
    setIsRunning(false);
    clearInterval(intervalRef.current);
  };

  const resetTimer = () => {
    if (isRunning) {
      pauseTimer();
    }
    if (time !== 0) {
      addSavedTimes(formatTime(time));
    }
    setTime(0);
  };

  const clearTimes = () => {
    setSavedTimes([]);
  };

  const addSavedTimes = (time) => {
    setSavedTimes((prevSavedTimes) => {
      const newSavedTimes = [...prevSavedTimes, time];
      if (newSavedTimes.length > MAX_SAVED_TIMES) {
        return newSavedTimes.slice(newSavedTimes.length - MAX_SAVED_TIMES)
      } else {
        return newSavedTimes;
      }
    })
  }

  const resetButton = (
    <button
      className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded ml-4"
      onClick={resetTimer}
    >
      Reset
    </button>
  )

  return (
    <div className="h-screen bg-white dark:bg-gray-800 pt-16 sm:pt-28 text-center text-white">
      <div className=''>
        <h1 className="text-6xl sm:text-8xl md:text-9xl font-bold mb-8">{formatTime(time)}</h1>
        {!isRunning ? (
          <>
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              onClick={startTimer}
            >
              Start
            </button>
            {time !== 0 && resetButton}
          </>
        ) : (
          <>
            <button
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
              onClick={pauseTimer}
            >
              Pause
            </button>
            {isRunning && resetButton}
          </>
        )}
        {savedTimes.length > 0 && (
          <button
            className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded ml-4"
            onClick={clearTimes}
          >
            Clear
          </button>
        )}
        {savedTimes.length > 0 && (
          <div className="mt-8">
            <h2 className="text-lg sm:text-xl font-bold mb-4">Saved Times:</h2>
            <ul>
              {savedTimes.map((savedTime, index) => (
                <li key={index} className="text-lg">
                  {savedTime}
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </div>
  );
}

export default Timer;